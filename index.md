---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: "John's Recipes"
---

Hi there! My name is John and I like to bake. I update the recipes I use every time I cook, so this website serves as a living recipe book for me.